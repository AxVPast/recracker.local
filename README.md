# ReCracker.Local

retracker.local (http://rutracker.wiki/Retracker.local) implementation in NodeJS 12 and Docker.

# Benefits
- Dockerized
- Zero configuration
- No SQL/MySQL/SqLite required - no SQL injection possibility
- All parameters are checked by format and length
- Works with ArmV7 Linux
- May be fast
- Only minimal functionality, minimal dependencies
- Allow to search near (from local lan, not using router) hosts when in torrent set Private flag (DHT/REX/LPD is disabled)
- Does not keep state to hard drive, so reboot clean up all data
- Command line interface only

# Disadvantages
- *Not for public networks usage, use it only in local lan with good controlled hosts, means it is NOT DDOS (Hacker) Proof*
- Theoretical max registered torrents (info hashes) is limited by NodeJS hash map implementation. Means not more than 200.000 info hashes.
- Info hash can be added, but do not remove it, so potentially huge memory usage or DDOS attack using a lot of announce requests with garbage hashes
- Theoretical max peers is unlimited and not filtered when returns by /announce request. So request parameter numwant is ignored. 
- Request /scrape returns constants, all zero if info_hash is not registered, all one if it known (someone announce it)
- Does not keep state to hard drive, so reboot clean up all data
- May be log to stdout more data than required, means it is not for HiLoad 
- Does not work with IPv6

# Main goal - establish direct connection between 2 hosts in local network using Transmission
Used for connect 2 Transmission over the local network.

Beginning state:
- exist 2 Transmission daemons
- port forwarding from router with static ports, not so fast router with 100Mbit Ethernet port
- unreal big incomplete stalled torrent
- Data transfer between Transmission works as: Transmission 1 <==> switch 1Gbit/MTU 8000 <==> router 100Mbit/MTU 1500 <==> switch 1Gbit/MTU 8000 <==> Transmission 2

Desired behavior
- Data transfer between Transmission works as: Transmission 1 <==> switch 1Gbit/MTU 8000 <==> Transmission 2

# Use ReCracker local

Run ReCracker.local container (see ['Usage'] how to build and use Docker image)
```bash
make
make docker-run
```

Stop BOTH transmissions.
Modify torrent file in BOTH Transmissions. 
```bash
cd .transmission-daemon/torrents/
transmission-edit -a http://${ReCrackerHost}:3243/announce ${torrentFileToModify}
```

Run both transmissions:
- Preferred run order - first should run Transmission with more data (source Transmission) than another Transmission (destination Transmission), so first transmission is registered to ReCracker, second - read about it in ReCracker.

Look into Transmission web interface, and if all OK you see:
- Additional tracker: http://${ReCrackerHost}:3243
- ReCracker.local tracker show (constants): Seeders: 1, Leechers: 1, Downloads: 1
- ReCracker.local tracker tell that it got 1 peer (may be zero for short period of time, near 2 minutes, but You can "Ask tracker for more peers")
- In peer information you can see expected local IP addresses (but look into ["Side effects"])

# Side effects
Because you modify both torrent files and ADD additional tracker your Transmissions can use public IP and local IP (both).
Because Transmission peer selecting algorithm is "little bit strange":
- Sometime you continue download using public IP (router), you can see it in peers list.
- Sometime you see both another Transmission IP addresses - private and public and both transfers data.
- Sometime your Transmission loose in peer list another Transmission all addresses (WTF?).
- Sometime you 100% knows that one Transmission have good data block for another Transmission but it both is not connected to each other

*I have idea WHY:*
- *Short:* Transmission 1 upload data rate is more than Transmission 2 can eat (slow disk, big torrent pieces, encryption)
- Transmission 1 super fast upload big piece to Transmission 2 memory cache
- Transmission 2 begins recalculate checksums, save it to disc, decript ... and too busy to handle second piece from Transmission 1
- Transmission 1 see that packets is not processed by Transmission 2 and try slow down data rate from Transmission 1
- Transmission 1 & Transmission 2 decide, that peer data rates is too slow, may be peer is disappear AND remove another peer from peers list
- As result both Transmission does not see each other when physically located near each other using superfaset network connection and both have data to exchange

*Looks like a solution* 
- Slow down download data rate in Transmission 2 to it physical limitation.
- Transmissions restart helps little bit

Because my destination Transmission is choked with traffic, I do not check which MTU value used for data transfer

# Usage
Build Docker container
```bash
make docker-image
```

Build docker container with --no-cache
```bash
make NOCACHE=--no-cache docker-image
```

Run docker container as daemon
```bash
make docker-run
```

Run docker container in foreground
```bash
make DAEMON= docker-run
```

See logs from container
```bash
make docker-logs
```

Stop docker container
```bash
make docker-stop
```

# Normal logs
```bash
$ make DAEMON= docker-rundocker
container rm ReCrackerLocal || true
Error: No such container: ReCrackerLocal
docker container run --rm --name ReCrackerLocal -p 0.0.0.0:3243:3243 recracker-v1
System start SUCCESS. Server is listening on 3243
Register new Info hash: 1XXX2XXX2XXX4XXX5XXX1XXX2XXX2XXX4XXX5XXX
Register new peer: 192.168.1.111:1111 , peersCount: 1 for hash: 1XXX2XXX2XXX4XXX5XXX1XXX2XXX2XXX4XXX5XXX
Register new peer: 192.168.1.222:2222 , peersCount: 2 for hash: 1XXX2XXX2XXX4XXX5XXX1XXX2XXX2XXX4XXX5XXX
```

# Torrent modification
Add ReCracker.local annoncer entry:
```bash
transmission-edit -a http://${ReCrackerHost}:3243/announce ${torrentFileToModify}
```

# Configuration
In most off all cases it is not required. But you can edit index.js or look into Makefile (it provide some useful switches)

# Transmission versions
- Transmission 2.94 (d8e60ee44f)
- Transmission 2.82 (14206)
