const bencode = require( 'bencode' );

function btTrackerProtocolFactory( cfg,  btTrackerPeersChache ){

   // compact mode peers representation
   // ipv4 only
   function binEncodePeers( peersArray ){
        const buf = Buffer.alloc(peersArray.length * 6);
        var bufIndex = 0;
        for( let i in peersArray ){
            const ipAsString = peersArray[i].ip;
            if( ipAsString ){
                const ipSplitted = ipAsString.split(".").map( function (octet){ return parseInt( octet )});
                const portAsInt = parseInt(peersArray[i].port);
                buf.writeUInt8(ipSplitted[0], bufIndex++);
                buf.writeUInt8(ipSplitted[1], bufIndex++);
                buf.writeUInt8(ipSplitted[2], bufIndex++);
                buf.writeUInt8(ipSplitted[3], bufIndex++);
                buf.writeUInt8((portAsInt >> 8) & 0xFF, bufIndex++);
                buf.writeUInt8(portAsInt & 0xFF, bufIndex++);
            }
        }
        return buf;
    }

    function checkParameters( paramsDeclaration, query ){
        var errors = undefined;
        function err( errDescription ){
            if(!errors){
                errors = [];
            }
            errors.push( errDescription );
        };
        for( let pName in paramsDeclaration ){
            //console.log(`check ${pName}`, query, query[pName] );
            const declaration = paramsDeclaration[pName];
            const param = query[pName];
            if( !param ){
                if( declaration.undef ){
                    continue;
                }
                err(`Missing value for mandatory parameter '${pName}'.`);
                continue;
            }
            function checkSingleValue( paramStr ){
                if( !paramStr ){
                    if( !declaration.undef ){
                        err(`Missing value for mandatory parameter '${pName}'.`);
                    }
                    return;
                }
                if( declaration.maxLength && paramStr.length > declaration.maxLength){
                    err(`Parameter '${pName}' length is more than allowed.`);
                    return;
                }
                if( declaration.regexp && (!declaration.regexp.test( paramStr ))){
                    err(`Parameter '${pName}' do not pass regexp:${declaration.regexp} check.`);
                    return;
                }
            }
            if(declaration.array){
                if( !Array.isArray (param) ){
                    err(`Parameter '${pName}' is not Array.`)
                    continue;
                }
                for( let i in param ){
                    checkSingleValue( param[i] );    
                }
            } else {
                checkSingleValue( param );
            } 
        }
        return errors;
    }

    function logErrorsIfPresent( errors ){
        if( !errors ){
            return false;
        }
        for( let i in errors ){
            console.log(errors[i]);
        }
        return true;
    }


    const announceParameters = {
        info_hash: { regexp:/^.{20}$/, maxLength:20},
        peer_id: { maxLength:20 },
        ip: {regexp: /^\d+\.\d+\.\d+\.\d+$/, maxLength:15, undef: true},
        port: {regexp:/^\d+$/, maxLength:5},
        event: {maxLength:20, undef:true },
        downloaded: {regexp:/^\d+$/,maxLength:50},
        left: {regexp:/^\d+$/,maxLength:50},
        compact: {maxLength:5, undef: true }
    }

    function announceHandler( ipFromNetAsString, q ){
        if( logErrorsIfPresent(checkParameters( announceParameters, q )) ){
            return {
                statusCode: 400,
                body: "Parameters check failed."
            }
        }
        const answer = btTrackerPeersChache.announce(
            //infoHash, peer_id, ipFromNetAsString, ipFromUrlAsString, portAsString, downloaded, left, event
            q.info_hash, q.peer_id, ipFromNetAsString, q.ip, q.port, parseInt(q.downloaded), parseInt(q.left), q.event
        )
        //console.log('Announce response:', answer );
        if( q.compact === '1'){
            answer.peers = binEncodePeers(  answer.peers );
        }
        return {
            body: bencode.encode( answer )
        }
    }

    const scrapeParameters = {
        info_hash: { array: true , regexp: /^.{20}$/ }
    }

    function scrapeHandler( ipFromNetAsString, query ){
        if( logErrorsIfPresent(checkParameters( scrapeParameters, query )) ){
            return {
                statusCode: 400,
                body: "Parameters check failed."
            }
        }
        const answer = btTrackerPeersChache.scrape( query.info_hash );
        return {
            body: bencode.encode( answer )
        }
    }

    function regsterHandlers( serverRegisterHandlers ){
        serverRegisterHandlers('/announce', announceHandler, announceParameters );
        serverRegisterHandlers('/announce.php', announceHandler, announceParameters );
        serverRegisterHandlers('/scrape', scrapeHandler, scrapeParameters );
        serverRegisterHandlers('/scrape.php', scrapeHandler, scrapeParameters );
    }

    return {
        regsterHandlers,
        visibleForTests: {
            binEncodePeers,
            announceHandler,
            scrapeHandler,

            checkParameters,
            logErrorsIfPresent
        }
    }
}

module.exports = btTrackerProtocolFactory;
