

function btTrackerPeersCacheFactory(cfg){

    const _log = console.log;
    const infoHashes = {};

    var dateNow = Date.now;
    var nextGcRunMs = 0;

    function setDateNow( func ){
        dateNow = func;
    };

    function binString2Hex( inString ){
        return Buffer.from(inString, 'ascii').toString('hex');
    }

    function autoRegisterPeer( infoHash, ipAsString, portAsString, timeNow ){
        const peerKey = ipAsString + ":" + portAsString;
        var currInfoHash = infoHashes[infoHash];
        if( ! currInfoHash ){
            currInfoHash = {
                peers: {},
                peersCnt: 0
            };
            infoHashes[infoHash] = currInfoHash;
            _log("Register new Info hash:", binString2Hex(infoHash) );
        }
        var currPeerContext = currInfoHash.peers[peerKey];
        if(!currPeerContext){
            currPeerContext = {
                ip: ipAsString, 
                port: portAsString
            }
            currInfoHash.peers[peerKey] = currPeerContext;
            currInfoHash.peersCnt++;
            _log("Register new peer:", peerKey, ', peersCount:',  currInfoHash.peersCnt , 'for hash:', binString2Hex(infoHash) );
        }
        currPeerContext.lastUpdateTime = timeNow;
        return {
            infoHashContext: currInfoHash,
            peerContext: currPeerContext
        }
    }

    /**
     * Return Alive peers, means modify cache and remove from it not Alive peers. Return only Alive peers.
     * It is unsafe, because it return all peers.
     * 
     * @param {*} infoHashContext 
     * @param {*} excludeIp caller IP
     * @param {*} gcTimeMs and be 0, means GC is not required or contains some value, means GC should be executed.
     */
    function getAlivePeers( infoHashContext, excludeIp, gcTimeMs ){
        var resPeerArray = [];
        for (let peerKey in infoHashContext.peers) {
            const currPeer = infoHashContext.peers[peerKey];
            if( gcTimeMs ){
                if( currPeer.lastUpdateTime < gcTimeMs ){
                    infoHashContext.peersCnt--;
                    delete infoHashContext.peers[peerKey];
                    _log(`Removed not Active peer:${peerKey}`);
                    continue;
                }
            }
            if(excludeIp != currPeer.ip){
                resPeerArray.push({
                    ip: currPeer.ip, 
                    port: currPeer.port,
                    'peer id': currPeer.peer_id
                })
            }
        }
        return resPeerArray;
    }

    /*
        ?info_hash=12345678901234567890 
        &peer_id=ABCDEFGHIJKLMNOPQRST
        &ip=255.255.255.255
        &port=6881
        &downloaded=1234
        &left=98765
        &event=stopped
        */
    function announce( infoHash, peer_id, ipFromNetAsString, ipFromUrlAsString, portAsString, downloaded, left, event ){
        const timeNow = dateNow();
        const { infoHashContext, peerContext } = autoRegisterPeer( infoHash, ipFromNetAsString, portAsString, timeNow );
        if( event ){
            peerContext.lastEvent = event;
        }
        peerContext.peer_id = peer_id;
        peerContext.downloaded = downloaded;
        peerContext.left = left;

        // GC
        var gcTimeMs = 0;
        if( nextGcRunMs < timeNow ){
            const gcIntervalMs = cfg.gcIntervalS * 1000;
            gcTimeMs = timeNow - gcIntervalMs;
            nextGcRunMs = timeNow + gcIntervalMs;
        }
        const peersArray = getAlivePeers( infoHashContext , ipFromNetAsString, gcTimeMs );
        return {
                'interval' : cfg.intervalS,
                'min interval' : cfg.minIntervalS,
                'complete' : 1,
                'incomplete' : 1,
                'downloaded' : 1,
                'peers' : peersArray
        }
    }

    function scrape( infoHashArray ){
        const files = {};
        for( let infoHashIndex in infoHashArray ){
            const infoHash = infoHashArray[infoHashIndex];
            const currInfoHash = infoHashes[infoHash];
            if( currInfoHash ){
                files[infoHash] = {
                    complete: 1, // number of peers with the entire file, i.e. seeders (integer)
                    downloaded: 1, // total number of times the tracker has registered a completion ("event=complete", i.e. a client finished downloading the torrent)
                    incomplete: 1 // number of non-seeder peers, aka "leechers" (integer)
                }
            } else {
                files[infoHash] = {
                    complete: 0, // number of peers with the entire file, i.e. seeders (integer)
                    downloaded: 0, // total number of times the tracker has registered a completion ("event=complete", i.e. a client finished downloading the torrent)
                    incomplete: 0 // number of non-seeder peers, aka "leechers" (integer)
                }
            }
        }
        return {
            files
        };
    }

    return {
        announce,
        scrape,
        visibleForTests: {
            getAlivePeers,
            autoRegisterPeer,
            setDateNow
        }

    }
}

module.exports = btTrackerPeersCacheFactory;