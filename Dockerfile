FROM node:12

WORKDIR /app/recracker

COPY package*.json ./

RUN npm install

# Copy without tests
COPY *.js ./

RUN useradd -ms /bin/bash  recracker

USER recracker

EXPOSE 3243

CMD [ "node", "index.js" ]