
NOCACHE=--no-cache
NOCACHE=
DAEMON=-d
LOGFOLLOW=-f
PORT=3243

.PHONY: docker-image docker-run docker-sh docker-logs docker-stop

docker-image:
	docker build $(NOCACHE) -t recracker-v1 .

docker-run:
	docker container rm ReCrackerLocal || true
	docker container run --rm --name ReCrackerLocal -p 0.0.0.0:$(PORT):3243 $(DAEMON) recracker-v1

docker-sh:
	docker container exec -it ReCrackerLocal /bin/bash

docker-logs:
	docker container logs $(LOGFOLLOW) ReCrackerLocal

docker-stop:
	docker container stop ReCrackerLocal
	docker container rm -f ReCrackerLocal || true

