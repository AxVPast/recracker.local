
const announceFactory = require('../bttrackerpeerscache');
const assert = require('assert');

const defaultTestConfig = { gcIntervalS: 120 * 4, intervalS: 120, minIntervalS: 10 };

describe('Announce functions.', () => {

    it('Should auto register new peer.', () => {
        const ann = announceFactory(defaultTestConfig).visibleForTests;
        const answer1 = ann.autoRegisterPeer('hash1', '1.2.3.4', '5678');
        const answer2 = ann.autoRegisterPeer('hash1', '1.2.3.4', '5678');
        assert.equal( answer1.infoHashContext, answer2.infoHashContext , 'Should return the same object');
        assert.equal( answer1.infoHashContext.peersCnt, 1 , 'Should see 1 peer.');
 
        const answer3 = ann.autoRegisterPeer('hash1', '5.6.7.8', '9101');
        assert.equal( answer1.infoHashContext, answer3.infoHashContext , 'Should return the same object.');
        assert.equal( answer3.infoHashContext.peersCnt, 2 , 'Should see 2 peers.');
 
        const answer4 = ann.autoRegisterPeer('hash2', '5.6.7.8', '9101', 1581233026452 );
        assert.notEqual( answer1.infoHashContext, answer4.infoHashContext , 'Different info hash');
        //console.log('currInfoHash4.peers:', currInfoHash4.peers);
        assert.deepStrictEqual( answer4.infoHashContext.peers['5.6.7.8:9101'], { ip:'5.6.7.8', port:'9101', lastUpdateTime: 1581233026452 } , 'Contain peer data.');
 
    });

    it('Should return peers list.', ()=>{
        const ann = announceFactory(defaultTestConfig).visibleForTests;
        const answer0 = ann.autoRegisterPeer('hash1', '1.2.3.4', '5678');
        answer0.peerContext.peer_id = 'xdr';
        const answer1 = ann.autoRegisterPeer('hash1', '1.2.3.6', '15678');
        answer1.peerContext.peer_id = 'abc';

        const peersArray = ann.getAlivePeers( answer1.infoHashContext );

        assert.deepStrictEqual( peersArray, [{ ip:'1.2.3.4', port:'5678', 'peer id':'xdr'},{ ip:'1.2.3.6', port:'15678', 'peer id':'abc'}])

    });

    it('Should remove not active peers (run peers GC).', ()=>{
      const ann = announceFactory(defaultTestConfig);
      // Date.now() == 1
      ann.visibleForTests.setDateNow( function (){ return 1 ;});
      // infoHash, peer_id, ipFromNetAsString, ipFromUrlAsString, portAsString, downloaded, left, event
      const answer1 = ann.announce('hash1', 'abc', '1.2.3.4', '11.2.3.4', '5678', '13', '180', 'stopped');

      // Date.now() == defaultTestConfig.gcIntervalS*1000+2
      // so prev announced peer should be removed
      ann.visibleForTests.setDateNow( function (){ return defaultTestConfig.gcIntervalS*1000+2 ;});
      const answer2 = ann.announce('hash1', 'xdr', '5.6.7.8', '15.6.7.8', '1678', '130', '18', 'stopped');
      
      //console.log('Answer2 is:', answer2 );
      assert.deepStrictEqual( answer2, {
          interval: 120,
          'min interval': 10,
          complete: 1,
          incomplete: 1,
          downloaded: 1,
          peers: []
        });
    });

    it('Should process retracker.local annnounce function.', ()=>{
        const ann = announceFactory(defaultTestConfig);
        ann.visibleForTests.setDateNow( function (){ return 0 ;});
        // infoHash, peer_id, ipFromNetAsString, ipFromUrlAsString, portAsString, downloaded, left, event
        const answer1 = ann.announce('hash1', 'abc', '1.2.3.4', '11.2.3.4', '5678', '13', '180', 'stopped');
        const answer2 = ann.announce('hash1', 'xdr', '5.6.7.8', '15.6.7.8', '1678', '130', '18', 'stopped');
        
        //console.log('Answer2 is:', answer2 );
        assert.deepStrictEqual( answer2, {
            interval: 120,
            'min interval': 10,
            complete: 1,
            incomplete: 1,
            downloaded: 1,
            peers: [
              { ip: '1.2.3.4', port: '5678', 'peer id': 'abc' }
            ]
          });
    });

    it('Should process retracker.local scrape function.', ()=>{
        const ann = announceFactory(defaultTestConfig);

        const answer1 = ann.scrape(['hash1']);
        assert.deepStrictEqual( answer1, {
               files: {
                 hash1: {
                   complete: 0,
                   downloaded: 0,
                   incomplete: 0
                 }
               }
        });

        // infoHash, peer_id, ipFromNetAsString, ipFromUrlAsString, portAsString, downloaded, left, event
        ann.announce('hash1', 'abc', '1.2.3.4', '11.2.3.4', '5678', '13', '180', 'stopped');

        const answer2 = ann.scrape(['hash1']);
        assert.deepStrictEqual( answer2, {
            files: {
              hash1: {
                complete: 1,
                downloaded: 1,
                incomplete: 1
              }
            }
     });

    });

});
