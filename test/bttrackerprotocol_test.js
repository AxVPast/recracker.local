
const assert = require('assert');

const btTrackerProtocolFactory = require('../bttrackerprotocol');
const btTrackerPeersCacheFactory = require('../bttrackerpeerscache');

describe("BT Tracker protocol.", ()=>{
    var btTrackerProtocol;
    var btTrackerPeersCache;
    before( () => {
        btTrackerPeersCache = btTrackerPeersCacheFactory({});
        btTrackerProtocol = btTrackerProtocolFactory({}, btTrackerPeersCache);
    });

    describe('Parameters validation engine.', () =>{

        function assertCheck( checkDeclaration, value , expectedError ){
            const checker = btTrackerProtocol.visibleForTests.checkParameters;
            const ret = checker({x:checkDeclaration}, {x:value});
            if( expectedError && ! ret ){
                assert.fail(`For ${checkDeclaration} for '${value}' expected error '${expectedError}', but check is passed.`)
            }
            if( (!expectedError) && (!ret)){
                return; // success
            }
            if( ret.length > 1 ){
                assert.fail(`For ${checkDeclaration} for '${value}' got more than one error '${ret}'.`)
            }
            assert.equal( ret[0], expectedError, `For ${checkDeclaration} for '${value} got not expected error.` );
        }

        it('Should fail check if expected array but got not array.', ()=>{
            assertCheck( {array:true}, ['string'] );
            assertCheck( {array:true}, 'string', "Parameter 'x' is not Array.");
        })

        it('Should fail check if value in array do not pass check.', ()=>{
            assertCheck( {array:true, maxLength: 30}, ['string'] );
            assertCheck( {array:true, maxLength: 3}, ['string'], "Parameter 'x' length is more than allowed.");
        })

        it('Should fail check if value do not pass regexp.', ()=>{
            assertCheck( {regexp: /^\d+\.\d+\.\d+.\d+$/}, '127.0.0.1' );
            assertCheck( {regexp: /^\d+\.\d+\.\d+.\d+$/}, 'string', "Parameter 'x' do not pass regexp:/^\\d+\\.\\d+\\.\\d+.\\d+$/ check.");
        })

        it('Should not fail check if value is not mandatory.', ()=>{
            assertCheck( {regexp: /^\d+\.\d+\.\d+.\d+$/, undef: true }, undefined );
        })

        it('Should log errors if present.' , () => {
            const logErrorsIfPresent = btTrackerProtocol.visibleForTests.logErrorsIfPresent;
            assert.equal(logErrorsIfPresent(undefined), false);
            assert.equal(logErrorsIfPresent(['Some error happens.']), true);
        })

    });

    it('Should process scrape request.', ()=>{
        const answer = btTrackerProtocol.visibleForTests.scrapeHandler('1.2.3.4', {
            info_hash: ['hash1hash2hash3hash4']
        });

        // toString('ascii') - truncates 8 bit, so it is not good for compact ip format for testing and comparisons.
        assert.equal( answer.body.toString('ascii'), "d5:filesd20:hash1hash2hash3hash4d8:completei0e10:downloadedi0e10:incompletei0eeee");
    });

    it('Should process announce request.', ()=>{
        const answer = btTrackerProtocol.visibleForTests.announceHandler('1.2.3.4', {
            info_hash: 'hash1hash2hash3hash4',
            ip: '5.6.7.8',
            port: '3215',
            peer_id: '222222',
            downloaded: '100',
            left: '687',
            event: 'stopped'
        });
        const answer2 = btTrackerProtocol.visibleForTests.announceHandler('11.2.3.4', {
            info_hash: 'hash1hash2hash3hash4',
            ip: '5.6.7.18',
            port: '3215',
            peer_id: '222222',
            downloaded: '100',
            left: '687',
            event: 'stopped'
        });
        // toString('ascii') - truncates 8 bit, so it is not good for compact ip format for testing and comparisons.
        assert.equal( answer2.body.toString('ascii'), "d8:completei1e10:downloadedi1e10:incompletei1e5:peersld2:ip7:1.2.3.47:peer id6:2222224:port4:3215eee");
    });

    it('Shold encode IPv4 addresses in compackt format.', ()=>{
        const binEncodePeers = btTrackerProtocol.visibleForTests.binEncodePeers;    
        const res = binEncodePeers([{ip:'10.10.10.5',port:'128'}]);
        // Example from here:
        // https://wiki.theory.org/index.php/BitTorrent_Tracker_Protocol
        assert.equal( res.toString('hex'), '0a0a0a050080')
    });

});