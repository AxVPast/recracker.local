
const btTrackerPeersCacheFactory = require('./bttrackerpeerscache');
const btTrackerProtocolFactory = require('./bttrackerprotocol');
const serverFactory = require('./server');

const cfg = {
    cache: { gcIntervalS: 120 * 4, intervalS: 120, minIntervalS: 10 },
    router: {},
    httpServer: { port:3243 }
}

function main(){
    const recracker = btTrackerPeersCacheFactory( cfg.cache );
    const router = btTrackerProtocolFactory( cfg.router, recracker );
    const server = serverFactory( cfg.httpServer, router );

    server.start();
}

main();