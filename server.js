const http = require('http');

function serverFactory( cfg , impl ){
    var server;
    const port = cfg.port;
    const handlers = {}; 

    function registerHandlers( urlPart, processingFunction , paramsNames ){
        handlers[urlPart] = { func: processingFunction, params: paramsNames };
    }

    impl.regsterHandlers( registerHandlers );

    function requestHandler(request, response){
        //console.log('Http Handle:', request.socket.remoteAddress , request.url );
        const parsedUrl = new URL(request.url, 'http://0.0.0.0:' + port );
        const urlPath = parsedUrl.pathname;
        const urlQuery = parsedUrl.searchParams;
        const handler = handlers[urlPath];

        const ipFromNetAsString = request.socket.remoteAddress;

        if( handler ){
            var funcArguments = {};
            for( let name in handler.params ){
                if( handler.params[name].array ){
                    funcArguments[name] = urlQuery.getAll( name );
                } else {
                    funcArguments[name] = urlQuery.get( name );
                }
            }
            //console.log(`Call ${urlPath} function with arguments:`, funcArguments);
            const resp = handlers[urlPath].func(ipFromNetAsString, funcArguments);
            response.statusCode = resp.statusCode || 200;
            response.end(resp.body);
            return;
        }
        console.log(`Unsupported HTTP request ${urlPath}` );
        response.end('Unsupported HTTP request', request.url );
      }
      
      function start(){
        server = http.createServer(requestHandler);
        server.listen(cfg.port || port, "0.0.0.0", (err) => {
            if (err) {
              console.log('Something bad happened', err)
              process.exit(0);
            }
          
            console.log(`System start SUCCESS. Server is listening on ${port}`)
          })
    
      }
 
      function stop(){
          server.stop();
          process.exit(0);
      }

      return {
          start: start,
          registerHandlers: registerHandlers,
          stop: stop
      }
}

module.exports = serverFactory;